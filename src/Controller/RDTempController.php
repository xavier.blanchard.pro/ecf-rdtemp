<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Officine;
use App\Entity\ChambreFroide;
use App\Entity\DataTemp;
use App\Form\OfficineType;
use App\Form\ChambreFroideType;
use App\Form\RegistrationType;
use App\Form\DataTempType;
use App\Form\UploadDataType;
use App\Form\ListesChoixType;
use App\Repository\UserRepository;
use App\Repository\OfficineRepository;
use App\Repository\ChambreFroideRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Query\AST\WhereClause;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class RDTempController extends AbstractController
{
    /**
     * @Route("/rdtemp", name="rd_temp")
     */
    public function index(): Response
    {
        return $this->render('rd_temp/index.html.twig', [
            'controller_name' => 'RDTempController',
        ]);
    }
    
    /**
     * @Route("/home", name="home")
     */
    public function home(): Response
    {
        return $this->render('rd_temp/home.html.twig');
    }

    /**
     * @Route("/listtech", name="listtech")
     */
    public function listtech(): Response
    {
        $role_cherche = "ROLE_TECHNICIEN";
        $repo = $this->getDoctrine()->getRepository(User::class);
        $techniciens = $repo->findByRole($role_cherche);

        return $this->render('admin/listtech.html.twig', [
            'techniciens' => $techniciens
        ]);
    }

    /**
     * @Route("/listofficine", name="listofficine")
     */
    public function listofficine(): Response
    {
        $role_cherche = "ROLE_OFFICINE";
        $repo = $this->getDoctrine()->getRepository(User::class);
        $officines = $repo->findByRole($role_cherche);
//dd($officines);
        return $this->render('admin/listofficine.html.twig', [
            'officines' => $officines
        ]);
    }

    /**
     * @Route("/createtech", name="createtech")
     */
    public function createtech(
        Request $request,
        EntityManagerInterface $manager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $technicien = new User();
        $technicien->setRoles(array("ROLE_TECHNICIEN"));

        $form = $this->createForm(RegistrationType::class, $technicien);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Création du User Tehnicien
            $technicien->setPassword(
                $passwordEncoder->encodePassword(
                    $technicien,
                    $form->get('password')->getData()
                )
            );
            $manager->persist($technicien);
            $manager->flush();

            return $this->redirectToRoute('listofficine');
        }

        return $this->render('admin/createtech.html.twig', [
            'formTechnicien' => $form->createView()
        ]);
    }

    /**
     * @Route("/updatetech/{id}", name="updatetech")
     */
    public function updatetech(
        $id,
        Request $request,
        EntityManagerInterface $manager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $tech = $repo->find($id);

        $form = $this->createForm(RegistrationType::class, $tech);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tech->setPassword(
                $passwordEncoder->encodePassword(
                    $tech,
                    $form->get('password')->getData()
                )
            );
            $manager->persist($tech);
            $manager->flush();

            return $this->redirectToRoute('listtech');
        }

        return $this->render('admin/updatetech.html.twig', [
            'formTech' => $form->createView()
        ]);
    }

    /**
     * @Route("/createofficine", name="createofficine")
     */
    public function createofficine(
        Request $request,
        EntityManagerInterface $manager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $officine = new User();
        $officine->setRoles(array("ROLE_OFFICINE"));

        $form = $this->createForm(RegistrationType::class, $officine);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Création du User Officine
            $officine->setPassword(
                $passwordEncoder->encodePassword(
                    $officine,
                    $form->get('password')->getData()
                )
            );
            $manager->persist($officine);
            $manager->flush();

            return $this->redirectToRoute('createofficine2');
        }

        return $this->render('admin/createofficine.html.twig', [
            'formUser' => $form->createView()
        ]);
    }

    /**
     * @Route("/createofficine2", name="createofficine2")
     */
    public function createofficine2(
        Request $request,
        EntityManagerInterface $manager
    ) {
        $officine = new Officine();
        
        $form = $this->createForm(OfficineType::class, $officine);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($officine);
            $manager->flush();

            return $this->redirectToRoute('listofficine');
        }

        return $this->render('admin/createofficine2.html.twig', [
            'formOfficine' => $form->createView()
        ]);
    }

    /**
     * @Route("/updateofficine/{id}", name="updateofficine")
     */
    public function updateofficine(
        $id,
        Request $request,
        EntityManagerInterface $manager
    ) {
        $repo = $this->getDoctrine()->getRepository(Officine::class);
        $officine = $repo->find($id);

        $form = $this->createForm(OfficineType::class, $officine);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //dd($officine);
            $manager->persist($officine);
            $manager->flush();

            return $this->redirectToRoute('listofficine');
        }

        return $this->render('admin/updateofficine.html.twig', [
            'formOfficine' => $form->createView()
        ]);
    }

    /**
     * @Route("/listchambrefroide", name="listchambrefroide")
     */
    public function listchambrefroide(): Response
    {
        $repo = $this->getDoctrine()->getRepository(ChambreFroide::class);
        $chambresfroides = $repo->findAll();
        return $this->render('admin/listchambrefroide.html.twig', [
            'chambresfroides' => $chambresfroides
        ]);
    }

    /**
     * @Route("/listchoixtest", name="listchoixtest")
     */
    public function listchoixtest(
        Request $request,
        EntityManagerInterface $manager
    )
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $resultat = $repo->findAll();

        $form = $this->createForm(ListesChoixType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            dd($resultat);
            $manager->persist($resultat);
            $manager->flush();
        }

        return $this->render('admin/testlistechoix.html.twig', [
            'formTest' => $form->createView()
        ]);
    }

    /**
     * @Route("/createchambrefroide", name="createchambrefroide")
     */
    public function createchambrefroide(
        Request $request,
        EntityManagerInterface $manager
    ) {
        $chambrefroide = new ChambreFroide();
        
        $form = $this->createForm(ChambreFroideType::class, $chambrefroide);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($chambrefroide);
            $manager->flush();

            return $this->redirectToRoute('listchambrefroide');
        }

        return $this->render('admin/createchambrefroide.html.twig', [
            'formChambreFroide' => $form->createView()
        ]);
    }

    /**
     * @Route("/updatechambrefroide/{id}", name="updatechambrefroide")
     */
    public function updatechambrefroide(
        $id,
        Request $request,
        EntityManagerInterface $manager
    ) {
        $repo = $this->getDoctrine()->getRepository(ChambreFroide::class);
        $chambrefroide = $repo->find($id);

        $form = $this->createForm(ChambreFroideType::class, $chambrefroide);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($chambrefroide);
            $manager->flush();

            return $this->redirectToRoute('listchambrefroide');
        }

        return $this->render('admin/updatechambrefroide.html.twig', [
            'formChambreFroide' => $form->createView()
        ]);
    }

    /**
     * @Route("/uploadfile", name="uploadfile")
     */
    public function uploadfile(
        Request $request,
        EntityManagerInterface $manager
    ) {
        $repo = $this->getDoctrine()->getRepository(ChambreFroide::class);
        $uploadData = $repo->findAll();

        $form = $this->createForm(UploadDataType::class, $uploadData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile */
            $file = $form->get('fileTemp')->getData();
            
            // Open the file
            if (($handle = fopen($file->getPathname(), "r")) !== false) {
                // Read and process the lines.
                // Skip the first line if the file includes a header
                while (($data = fgetcsv($handle)) !== false) {
                    // Do the processing: Map line to entity, validate if needed
                    $dataTemp = new DataTemp();
                    // Assign fields
                    $dataTemp->setDateHeure($data([0]))
                            ->setValeur($data([1]))
                            ->setChambreFroide($form->get('ChambreFroide')->getData());
                    $manager->persist($dataTemp);
                }
                fclose($handle);
                $manager->flush();
            }
            return $this->redirectToRoute('home');
        };

        return $this->render('tech/uploadfile.html.twig', [
            'formUpload' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/changepassword", name="changepassword")
     */
    public function changepassword(): Response
    {
        return $this->render('tech/changepassword.html.twig');
    }

    /**
     * @Route("/seecharts", name="seecharts")
     */
    public function seecharts(): Response
    {
        return $this->render('officine/seecharts.html.twig');
    }

    /**
     * @Route("/validcharts", name="validcharts")
     */
    public function validcharts(): Response
    {
        return $this->render('officine/validcharts.html.twig');
    }
}
