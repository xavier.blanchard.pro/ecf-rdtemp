<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Officine;
use App\Entity\ChambreFroide;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\EntityType;
use Symfony\Component\Form\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;

class ListesChoixType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', EntityType::class, [
                'class' => User::class,
                'mapped' => false,
                'choice_label' => 'Username',
                'placeholder' => 'Username',
                'label' => 'Username'
            ])

            ->add('officine', EntityType::class, [
                'class' => Officine::class,
                'mapped' => false,
                'choice_label' => 'RaisonSociale',
                'placeholder' => 'Officine',
                'label' => 'Officine'
            ])

            ->add('chambrefroide', EntityType::class, [
                'class' => ChambreFroide::class,
                'mapped' => false,
                'choice_label' => 'Nom',
                'placeholder' => 'Chambre Froide',
                'label' => 'Chambre Froide'
            ])


            ->add('Valider', SubmitType::class, [])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
