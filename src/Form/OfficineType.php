<?php

namespace App\Form;

use App\Entity\Officine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class OfficineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('RaisonSociale')
            ->add('Adresse')
            ->add('CodePostal')
            ->add('Ville')
            ->add('Telephone')
            ->add('User')
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Officine::class,
        ]);
    }
}
