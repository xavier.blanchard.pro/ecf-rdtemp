<?php

namespace App\Form;

use App\Entity\ChambreFroide;
use App\Entity\Officine;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ChambreFroide', EntityType::class, [
                'class' => 'App\Entity\ChambreFroide',
                'placeholder' => 'Sélectionner une Chambre Froide'
            ])
            ->add('FileTemp', FileType::class)
            ->add('FileHygro', FileType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $defaults = array();
    
        $resolver->setDefaults($defaults);
    }
}
