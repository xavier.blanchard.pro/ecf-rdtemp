<?php

namespace App\Repository;

use App\Entity\DataHygro;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DataHygro|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataHygro|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataHygro[]    findAll()
 * @method DataHygro[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataHygroRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataHygro::class);
    }

    // /**
    //  * @return DataHygro[] Returns an array of DataHygro objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataHygro
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
