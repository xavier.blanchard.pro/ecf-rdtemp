<?php

namespace App\Repository;

use App\Entity\DataTemp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DataTemp|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataTemp|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataTemp[]    findAll()
 * @method DataTemp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataTempRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataTemp::class);
    }

    // /**
    //  * @return DataTemp[] Returns an array of DataTemp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataTemp
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
